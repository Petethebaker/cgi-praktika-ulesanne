package com.cgi.dentistapp.dto;

import org.springframework.format.annotation.DateTimeFormat;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Date;


public class DentistVisitDTO {
	
	@Size(min = 2)
	String doctorName;

    @Size(min = 1, max = 50)
    String dentistName;

    @NotNull
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    Date visitDate;
    
    @NotNull
    @DateTimeFormat(pattern = "H:m")
    Date visitTime;


	public DentistVisitDTO() {
    }

    public DentistVisitDTO(String doctorName, String dentistName, Date visitDate, Date visitTime) {
    	this.doctorName = doctorName;
        this.dentistName = dentistName;
        this.visitDate = visitDate;
        this.visitTime = visitTime;
    }

    
    public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

    public String getDentistName() {
        return dentistName;
    }

    public void setDentistName(String dentistName) {
        this.dentistName = dentistName;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }
    
	public Date getVisitTime() {
		return visitTime;
	}

	public void setVisitTime(Date visitTime) {
		this.visitTime = visitTime;
	}
}
