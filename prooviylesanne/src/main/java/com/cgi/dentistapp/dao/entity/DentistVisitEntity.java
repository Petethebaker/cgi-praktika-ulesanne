package com.cgi.dentistapp.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.cgi.dentistapp.dto.DentistVisitDTO;

@Entity
@Table(name = "dentist_visit")
public class DentistVisitEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "visit_time")
    private Date visitTime;
    
    @Column(name = "visit_date_string")
    private String visitDateString;

    @Column(name = "visit_time_string")
    private String visitTimeString;
    
    @Column(name = "dentist_name")
    private String dentistName;
    
    @Column(name = "doctor_name")
    private String doctorName;
    

    public DentistVisitEntity() {
    }

    public DentistVisitEntity(DentistVisitDTO visit) {
    	setProperTime(visit);
    	this.dentistName = visit.getDentistName();
    	this.doctorName = visit.getDoctorName();
    }
    
    /**
     * Kui veebilehel uuendada visiiti siis tehakse uus DentistVisitDTO objekt
     * 
     * See meetod on selleks et infot uuendada uue objekti kohaselt
     */
    public void updateVisitInfo(DentistVisitDTO visit) {
    	setProperTime(visit);
    	this.dentistName = visit.getDentistName();
    	this.doctorName = visit.getDoctorName();
    }

    /**
     * Paneb DentistVisitDTO objektilt saadud kuupäeva ja aja Date objektid kokku 
     * ja teeb inimsõbralikud stringid kuupävast ja kellaajast, et hiljem parsima ei peaks
     * 
     * SupressWarnings, sest projekti oli alustatud Date objektidega ja ma ei hakanud seda muutma
     * Date objekti get ja set meetoid on kõik deprecated, aga töötavad
     * Liidab kuule +1 sest kuid loetakse 0...11 ja aastale 1900 sest sellest hakatakse lugema
     */
	@SuppressWarnings("deprecation")
	private void setProperTime(DentistVisitDTO visit) {
		this.visitTime = visit.getVisitDate();
    	this.visitTime.setHours(visit.getVisitTime().getHours());
    	this.visitTime.setMinutes(visit.getVisitTime().getMinutes());
    	
		this.visitDateString = visitTime.getDate() + "/" + (visitTime.getMonth() + 1) + "/"
				+ (visitTime.getYear() + 1900);

		this.visitTimeString = visitTime.getHours() + ":" + visitTime.getMinutes();
	}


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public Date getVisitTime() {
		return visitTime;
	}

	public void setVisitTime(Date visitTime) {
		this.visitTime = visitTime;
	}

	public String getDentistName() {
		return dentistName;
	}

	public void setDentistName(String dentistName) {
		this.dentistName = dentistName;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

}
