package com.cgi.dentistapp.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

import java.util.List;

@Repository
public class DentistVisitDao {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Lisab objekti andmebaasi
     */
    public void create(DentistVisitEntity visit) {
        entityManager.persist(visit);
    }

    /**
     * Võtab kõik objektid andmebaasist ja tagastab listina
     * 
     * SupressWarinings - sest ei saa tulla muude objektidega täidetud listi
     */
    @SuppressWarnings("unchecked")
	public List<DentistVisitEntity> getAllVisits() {
        return entityManager.createQuery("SELECT e FROM DentistVisitEntity e").getResultList();
    }
    
    /**
     * Võtab ühe objekti (mille primaarvõti on id) andmebaasist ja tagastab selle
     */
    public DentistVisitEntity getVisitInfo(Long id) {
    	return entityManager.find(DentistVisitEntity.class, id);
    }
    
    /**
     * Kustutab objekti (mille primaarvõti on id) andmebaasist
     */
    public void deleteVisit(Long id) {
    	entityManager.remove(entityManager.find(DentistVisitEntity.class, id));
    }
}
