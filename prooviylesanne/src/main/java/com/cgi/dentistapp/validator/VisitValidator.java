package com.cgi.dentistapp.validator;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.service.DentistVisitService;

@Component
public class VisitValidator implements Validator {

	@Autowired
	private DentistVisitService dentistVisitService;

	@Override
	public boolean supports(Class<?> clazz) {
		return DentistVisitDTO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		DentistVisitDTO visit = (DentistVisitDTO) target;
		checkDentisFirstAndLastName(errors, visit);
		checkIfDateBeforeToday(errors, visit);
		checkIfTimeTaken(errors, visit);
	}
	

	/**
	 * Teeb uue Date objekti kus on kuupäev ja aeg koos ning võrdelb seda 
	 * kõikide andmebaasis olevate visiitidega
	 * 
	 * Kui vahe on väiksem kui 1 800 000 ms (30 minuti), siis aeg ei sobi
	 */
	@SuppressWarnings("deprecation")
	private void checkIfTimeTaken(Errors errors, DentistVisitDTO visit) {
		Date visitTime = visit.getVisitDate();
		visitTime.setHours(visit.getVisitTime().getHours());
		visitTime.setMinutes(visit.getVisitTime().getMinutes());
		
		for (DentistVisitEntity entity : dentistVisitService.listVisits()) {
			if (visit.getDentistName().equals(entity.getDentistName())) {
				if (visitTime.getTime() - entity.getVisitTime().getTime() <= 1800000) {
					errors.rejectValue("visitTime", "time.already.taken");
				}
			}
		}
	}

	/**
	 * Vaatab et valitud kuupäev ei oleks enne tänast
	 */
	private void checkIfDateBeforeToday(Errors errors, DentistVisitDTO visit) {
		if (visit.getVisitDate() != null && visit.getVisitDate().before(new Date())) {
			errors.rejectValue("visitDate", "date.before.today");
		}
	}

	/**
	 * Vaatab et hambaarstil oleks ees- ja perekonnanimi (string sisaldaks tühikut)
	 */
	private void checkDentisFirstAndLastName(Errors errors, DentistVisitDTO visit) {
		if (!visit.getDentistName().contains(" ")) {
			errors.rejectValue("dentistName", "dentist.name.onlyOneName");
		}
	}

}
