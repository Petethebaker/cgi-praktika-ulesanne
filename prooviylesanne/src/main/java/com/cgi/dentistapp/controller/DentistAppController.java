package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dto.DentistVisitDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.cgi.dentistapp.service.DentistVisitService;
import com.cgi.dentistapp.validator.VisitValidator;
import com.google.gson.Gson;

import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.validation.Valid;

@Controller
@EnableAutoConfiguration
public class DentistAppController extends WebMvcConfigurerAdapter {

	@Autowired
	private DentistVisitService dentistVisitService;

	@Autowired
	private VisitValidator visitValidator;

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/results").setViewName("results");
		registry.addViewController("/list").setViewName("list");
		registry.addViewController("/edit").setViewName("edit");
	}

	@GetMapping("/")
	public String showRegisterForm(DentistVisitDTO dentistVisitDTO) {
		return "form";
	}

	@PostMapping("/")
	public String postRegisterForm(@Valid DentistVisitDTO visit, BindingResult bindingResult) {
		if (!validateForm(visit, bindingResult)) {
			return "form";
		}
		dentistVisitService.addVisit(visit);
		return "redirect:/results";
	}

	
	@GetMapping("/list")
	public String getVisitsList() {
		return "list";
	}
	

	@GetMapping(value = "/getList")
	@ResponseBody
	public String getListOfVisits() {
		return new Gson().toJson(dentistVisitService.listVisits());
	}
	

	@GetMapping(value = {"/getInfo/{id}", "edit/getInfo/{id}"})
	@ResponseBody
	public String getInfoAboutVisit(@PathVariable("id") int id) {
		return new Gson().toJson(dentistVisitService.getVisitInfo(Long.valueOf(id)));
	}
	
	@DeleteMapping("/deleteVisit/{id}")
	@ResponseBody
	public void deleteVisit(@PathVariable("id") int id) {
		dentistVisitService.deleteVisit(Long.valueOf(id));
	}
	
	@GetMapping("/edit/{id}")
	public String showEditForm(DentistVisitDTO dentistVisitDTO, @PathVariable("id") int id) {
		return "edit";
	}
	
	@PostMapping("/edit/{id}")
	public String updateVisit(@Valid DentistVisitDTO visit, BindingResult bindingResult, @PathVariable("id") int id) {
		if (!validateForm(visit, bindingResult)) {
			return "edit";
		}
		dentistVisitService.updateVisit(Long.valueOf(id), visit);
		return "redirect:/list";
	}
	

	/**
	 * Valideerib kõigepealt ära DentistVisitDTO objekti annotatsioonidega ja siis ise koostatud validatoriga
	 * 
	 * return false, kui leiti mõni viga, return true kui kõik oli korras
	 */
	private boolean validateForm(DentistVisitDTO visit, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return false;
		}
		visitValidator.validate(visit, bindingResult);
		if (bindingResult.hasErrors()) {
			return false;
		}
		return true;
	}
}
