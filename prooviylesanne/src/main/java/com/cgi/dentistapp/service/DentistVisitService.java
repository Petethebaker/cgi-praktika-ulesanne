package com.cgi.dentistapp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cgi.dentistapp.dao.DentistVisitDao;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;
import com.cgi.dentistapp.dto.DentistVisitDTO;

@Service
@Transactional
public class DentistVisitService {

    @Autowired
    private DentistVisitDao dentistVisitDao;

    public void addVisit(DentistVisitDTO visit) {
        dentistVisitDao.create(new DentistVisitEntity(visit));
    }

    public List<DentistVisitEntity> listVisits() {
        return dentistVisitDao.getAllVisits();
    }
    
    public DentistVisitEntity getVisitInfo(Long id) {
    	return dentistVisitDao.getVisitInfo(id);
    }
    
    public void deleteVisit(Long id) {
    	dentistVisitDao.deleteVisit(id);
    }
    
    public void updateVisit(Long id, DentistVisitDTO visit) {
    	getVisitInfo(id).updateVisitInfo(visit);
    }

}
